title: List contents of directory
script: list_contents_of_directory.sh
requires:
  - bash
---

Lists the content of a directory.
