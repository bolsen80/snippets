 #!/usr/bin/env python3

import os, sys, stat, yaml
from string import Template

from termcolor import cprint, colored

from rich.console import Console
from rich.markdown import Markdown

import whoosh.index as index
from whoosh.fields import Schema, TEXT, KEYWORD, ID, STORED
from whoosh.analysis import StemmingAnalyzer
from whoosh.qparser import QueryParser

DOCS_PATH = "docs"
SCRIPTS_PATH = "scripts"
INDEX_PATH = "snippets_index"
IGNORE_FILES=[
    'index.md',
    'about.md'
]

DOC_TEMPLATE = """---
title: $title
script: $script
requires:
  - bash
---

Add description here.
"""

MKDOCS_TEMPLATE = """site_name: $site_name
theme: readthedocs
plugins:
 - search
 - snippet_doc
"""

LIST_TEMPLATE = """- $title
 * $path
 * $key
"""
INDEX_SCHEMA = Schema(
    title=TEXT(stored=True),
    body=TEXT(stored=True, analyzer=StemmingAnalyzer()),
    filename=ID(stored=True, unique=True))


"""
Commands
"""

def new_project(project_name, with_git=False):
    os.mkdir(project_name)
    os.mkdir(os.path.join(project_name, "scripts"))
    os.mkdir(os.path.join(project_name, "docs"))

    with open(os.path.join(project_name, "mkdocs.yml"), "w") as mkdocs:
        mkdocs.write(Template(MKDOCS_TEMPLATE).substitute(dict(
            site_name=titleize(project_name))))

    if with_git:
        with open(os.path.join(project_name, ".gitignore"), "w") as gitignore:
            gitignore.write(INDEX_PATH)
        os.system("cd " + project_name + " ; git init; git add .; git commit -m \"Initial commit\"")
    print("Project generated: " + os.linesep + os.path.abspath(project_name))

def new_doc(title):
    """
    Inserts a new doc and script file.
    """

    docfile_path = os.path.join(DOCS_PATH, doc_file(snakecase(title)))
    scriptfile_path = os.path.join(SCRIPTS_PATH, script_file(snakecase(title)))

    with open(docfile_path, "w") as docfile:
        docfile.write(Template(DOC_TEMPLATE).substitute(dict(title=titleize(title), script=scriptfile_path)))

    with open(scriptfile_path, "w") as scriptfile:
        scriptfile.write("#!/bin/bash")
    os.chmod(scriptfile_path, stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)

    print("Files generated: ")
    print(os.path.abspath(docfile_path))
    print(os.path.abspath(scriptfile_path))

def list_snippets():
    """
    Lists all the files with filenames in the project.
    """
    print()
    for filename in os.listdir("docs"):
        if filename in IGNORE_FILES:
            continue

        with open(os.path.join("docs", filename), "r") as docfile:
            fm = get_front_matter(docfile.read())
            print(Template(LIST_TEMPLATE).substitute(
                dict(
                    title=fm['title'],
                    path=os.path.abspath(os.path.join("docs", filename)),
                    key=filename.split(".")[0]
                )))

def list_keys():
    """
    Lists all the keys in a project. Intended for completions.
    """
    for filename in os.listdir("docs"):
        if filename in IGNORE_FILES:
            continue
        print(filename.split(".")[0])



def view_docs(key):
    with open(os.path.join(DOCS_PATH, doc_file(key))) as docfile:
        docdata = docfile.read()
        fm = get_front_matter(docdata)
        cprint(fm['title'], "white", attrs=["bold"])
        console = Console()
        md = Markdown(get_doc_description(docdata))
        print()
        console.print(md)
        print("--")
        print(fm)

def search(search_string):
    print("Search String: " + search_string)
    if not os.path.exists("snippet_index"):
        print("Docs not indexed, starting indexing ...")
        index_repo()

    ix = index.open_dir("snippet_index")
    with ix.searcher() as searcher:
        qp = QueryParser("body", schema=INDEX_SCHEMA)
        q = qp.parse(search_string)
        results = searcher.search(q)

        count = 1
        for result in results:
            if result is not None:
                print(str(count) + ") " + colored(result['title'], "white", attrs=["bold"]))
            count = count + 1

        if len(results) > 0:
            num = input("View # ")
            view_docs(results[int(num)-1]['filename'].split(".")[0])

def run(key, args):
    run_check_file()

    with open(os.path.join(DOCS_PATH, doc_file(key))) as docfile:
        doc = docfile.read()
        fm = get_front_matter(doc)
        if len(args) > 0:
            os.system(os.path.join(fm['script']) + " " + " ".join(args))
        else:
            os.system(os.path.join(fm['script']))
"""
Helpers
"""

def snakecase(string):
    return string.lower().replace(" ", "_")

def titleize(string):
    return " ".join([s.capitalize() for s in string.split("_")])

def doc_file(key):
    return key + ".md"

def script_file(key):
    return key + ".sh"

def get_front_matter(data):
    fm = data.split("---")[1]
    return yaml.load(fm)

def get_doc_description(data):
    desc = data.split("---")[2]
    return desc

def is_project_dir():
    if os.path.exists("docs") and os.path.exists("scripts"):
        return True
    return False

def index_repo():
    if not os.path.exists("snippet_index"):
        os.mkdir("snippet_index")
        ix = index.create_in("snippet_index", schema=INDEX_SCHEMA)
    else:
        ix = index.open_dir("snippet_index", schema=INDEX_SCHEMA)
    writer = ix.writer()
    for filename in os.listdir("docs"):
        if filename in IGNORE_FILES:
            continue

        with open(os.path.join("docs", filename), "r") as docfile:
            docdata = docfile.read()
            fm = get_front_matter(docdata)
            body = get_doc_description(docdata)
            writer.add_document(title=fm['title'],
                                body=body,
                                filename=filename)
    writer.commit()

def run_check_file():
    if os.path.exists(os.path.join(SCRIPTS_PATH, "check_env.sh")):
        code = os.system(os.path.join(SCRIPTS_PATH, "check_env.sh"))
        if code != 0:
            sys.exit(code)
