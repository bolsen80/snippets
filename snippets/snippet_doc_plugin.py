#!/usr/bin/env python3

import os
from string import Template

import mkdocs

MD_SCRIPT_ADDITION="""

# Script
```
$script
```
"""

class SnippetDocPlugin(mkdocs.plugins.BasePlugin):
    def on_page_markdown(self, markdown, page, config, files):
        if page.meta.get('script') is None:
            return markdown

        with open(os.path.join(page.meta['script']), "r") as scriptfile:
            script = scriptfile.read()
        return markdown + Template(MD_SCRIPT_ADDITION).substitute(dict(script=script))
