#!/usr/bin/env python3
import sys

import click

from . import snippet

def pre_cmd_check():
    if not snippet.is_project_dir():
        click.echo("ERROR: directory is not a project directory.")
        sys.exit(1)

@click.group()
def cli():
    pass

@cli.command()
@click.argument("name")
@click.option('--git/--no-git', default=False, help="Create a git repository.")
def new(name, use_git):
    """
    Creates a new project.
    """
    snippet.new_doc(name, use_git)

@cli.command()
@click.argument("name")
def add(name):
    """
    Adds a new entry into a repository.
    """
    pre_cmd_check()
    snippet.new_doc(name)

@cli.command()
@click.argument("search_string")
def search(search_string):
    """
    Find entries with a search string.
    """
    pre_cmd_check()
    snippet.new_doc(search_string)


@cli.command()
def list():
    """
    List all documents in the repository.
    """
    pre_cmd_check()
    snippet.list_snippets()

@cli.command()
@click.argument("key")
def view(key):
    """
    View a single entry's doc.
    """
    pre_cmd_check()
    snippet.view_docs(key)

@cli.command()
@click.argument("key")
@click.argument("args", nargs=-1)
def run(key, args):
    """
    Execute a script related to an entry.
    """
    pre_cmd_check()
    snippet.run(key, args)

@cli.command()
def index():
    """
    Force a re-index of the repository.
    """
    pre_cmd_check()
    snippet.index_repo()

@cli.command()
def list_keys():
    """
    List all the entry keys. Can be used for shell completions, for example.
    """
    pre_cmd_check()
    snippet.list_keys()

def main():
    cli()
    
if __name__ == "__main__":
    cli()
