# Snippets

## Goal

There are many scripts that are used to do a lot of common things. Remembering them is not easy and having a big list of them is not
scalable. making it searchable and available in a terminal is easier.

I want to:

 1. See and write docs in markdown about certain tasks
 2. Run related scripts through it
 3. Type in a search query, like : `snippet search "my dashboard"`
    * It will come up with a list of results if more than one result is found.
    * If there is more than one result, it will list and ask what should be shown next. (You could also run the entry number with like `snippet run 12345`)
    * The doc will show up with the snippet. You can execute the script directly from there.

I also want to see the same documentation in another format. For this, using [MkDocs](https://www.mkdocs.org) is useful. The workflow roughly extends mkdocs but adds console views and workflows.

## Doc format

The documentation format is Markdown. Scripts are added separately and linked specially so the document parser can figure out what's the script.

For example, I would write:

``` markdown
---
title: View K8s dashboard
script: k8s-dashboard.sh
requires:
  - bash
  - google-chrome
  - ssh
---

Launches the K8s dashboards in a browser window, in multiple tabs. Use this to find services and look at their logs.
```

In it, there is a title, a link to the script (which is found in the $PROJECT/scripts root) and a list of required commands that is checked with a simple bash script to see
if the commands exist (this is optional, but can be user-friendly.)


## Workflow

### I want to add a new documentation project

``` shell
$ snippet new project_name
```

### I want to add a new doc

``` shell
$ cd project_name && snippet add "Bootstrap a new server"
Files created:
$PROJECT/docs/bootstrap-a-new-server.md
$PROJECT/scripts/bootstrap-a-new-server.sh

$ nano $PROJECT/docs/bootstrap-a-new-server.md
```

### I want to search for a command

``` shell
$ snippet search "bootstrap"
Choose:
1. Bootstrap new server
2. Bootstrap new project
? 1
Bootstrapping a new server involves sending a request to the cloud API to generate the requst.

Proceed? (y/n) y
[... output from script ...]
```

### I want to just run a known command

Just call the script:
``` shell
$ ./scripts/bootstrap-a-new-server.sh
```

### I want to instead have an external script run but make it accessible

In the doc, specify the full path to the script:

``` shell
title: Run a config repo command
script: /config/code/bin/bootstrap.sh
---
Runs an external command
```

### Example of creating snippet and seeing

``` shell
$ snippet add "List contents of directory"
Files generated:
/home/user/snippet/sample/docs/list_contents_of_directory.md
/home/user/snippet/sample/scripts/list_contents_of_directory.sh

$ ls -l scripts
total 4
-rwx------ 1 bolsen bolsen 11 jan.   4 16:52 list_contents_of_directory.sh

$ snippet list

- List contents of directory
 * /home/user/snippet/sample/docs/list_contents_of_directory.md
 * list_contents_of_directory

$ nano docs/list_contents_of_directory.md # Edit the markdown

$ snippet view list_contents_of_directory
List contents of directory

Lists the content of a directory.

--
{'title': 'List contents of directory', 'script': 'list_contents_of_directory.sh', 'requires': ['bash']}


$ snippet run list_contents_of_directory .
docs  scripts

```

### I want spin up a doc server

``` shell
$ mkdocs serve &
$ firefox https://127.0.0.1:8080
```
