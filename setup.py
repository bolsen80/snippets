from setuptools import setup

setup(
    name='snippet',
    version='0.0.1',
    description="Organize snippets, make them searchable, etc.",
    url="https://gitlab.com/bolsen80/snippets.git",
    author="Brian Olsen",
    author_email="brian@bolsen.org",
    package_dir={'': '.'},
    install_requires=[
        'rich==10.16.2',
        'click==7.1.2',
        'mkdocs==1.2.3',
        'whoosh==2.7.4'
    ],
    entry_points = {
        'console_scripts': ['snippet=snippets.cli:main'],
        'mkdocs.plugins': [
            'snippet_doc=snippets.snippet_doc_plugin:SnippetDocPlugin'
        ]
    }
)
